// SPDX-License-Identifier: (GPL-2.0 OR MIT)
/*
 * Copyright 2018 Toradex AG
 */

/dts-v1/;

#include "imx6ull-colibri-nonwifi.dtsi"

/ {
	model = "Toradex Colibri iMX6ULL 512MB on VIOLET GRIPS";
	compatible = "cubesatnb,colibri-imx6ull-GRIPS",
	             "toradex,colibri-imx6ull",
				 "fsl,imx6ull";
	
	chosen {
		bootargs = "console=ttymxc0,115200";
	};
	
	reg_3v3: regulator-3v3 {
		compatible = "regulator-fixed";
		regulator-name = "3.3V_COM";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		regulator-always-on;
	};
	
	reg_usbh_vbus: regulator-usbh-vbus {
		compatible = "regulator-fixed";
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_usbh_reg>;
		regulator-name = "3V3_OEM719";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
		gpio = <&gpio1 2 GPIO_ACTIVE_HIGH>, <&gpio1 3 GPIO_ACTIVE_HIGH>;
		vin-supply = <&reg_3v3>;
	};
};

&usdhc1 {
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_usdhc1>;
		status = "okay";
		non-removable;
		/delete-property/ pinctrl-1;
		/delete-property/ pinctrl-2;
		/delete-property/ pinctrl-3;
		/delete-property/ cd-gpios;
		/delete-property/ keep-power-in-suspend;
};

&uart1 {
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_uart1>;
		status = "okay";	
};

&uart2 {
		status = "okay";
};

&can1{
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_flexcan1>;
		status = "okay";
};

&can2 {
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_flexcan2>;
		status = "okay";
};

&ecspi1 {
		pinctrl-0 = <&pinctrl_ecspi1_cs &pinctrl_ecspi1>;
		status = "okay";
		spi-slave;
		/delete-property/ cs-gpios;
		/delete-node/spidev;
		
		slave@0 {
			compatible = "toradex,evalspi";
			spi-max-frequency = <500000>;
			reg = <0>;
			status = "okay";
		};
};

&usbotg2 {
		vbus-supply = <&reg_usbh_vbus>;
		status = "okay";
};

&i2c1 {
		status = "okay";
};
	
&i2c2 {
		status = "disabled";
};

&iomuxc {
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_OEM719_gpio1>;
		
		imx6ull-colibri{
			pinctrl_uart1: uart1-grp {
				fsl,pins = <
					MX6UL_PAD_UART1_TX_DATA__UART1_DTE_RX	0x1b0b1	/* SODIMM 33 */
					MX6UL_PAD_UART1_RX_DATA__UART1_DTE_TX	0x1b0b1	/* SODIMM 35 */
				>;
			};
			
			pinctrl_usdhc1: usdhc1-grp {
				fsl,pins = <
					MX6UL_PAD_SD1_CLK__USDHC1_CLK		0x10059 /* SODIMM 47 */
					MX6UL_PAD_SD1_DATA1__USDHC1_DATA1	0x17059 /* SODIMM 49 */
					MX6UL_PAD_SD1_DATA2__USDHC1_DATA2	0x17059 /* SODIMM 51 */
					MX6UL_PAD_SD1_DATA3__USDHC1_DATA3	0x17059 /* SODIMM 53 */
					MX6UL_PAD_SD1_CMD__USDHC1_CMD		0x17059 /* SODIMM 190 */
					MX6UL_PAD_SD1_DATA0__USDHC1_DATA0	0x17059 /* SODIMM 192 */
				>; 
			};
			
			pinctrl_flexcan1: flexcan1-grp {
				fsl,pins = <
					MX6UL_PAD_ENET1_RX_DATA0__FLEXCAN1_TX	0x1b020  /* SODIMM 55 */
					MX6UL_PAD_ENET1_RX_DATA1__FLEXCAN1_RX	0x1b020  /* SODIMM 63 */
				>;
			};
			
			pinctrl_flexcan2: flexcan2-grp {
				fsl,pins = <
					MX6UL_PAD_ENET1_RX_EN__FLEXCAN2_TX		0x1b020 /* SODIMM 178 */
					MX6UL_PAD_ENET1_TX_DATA0__FLEXCAN2_RX	0x1b020 /* SODIMM 188 */
				>;
			};
			
			pinctrl_uart2: uart2-grp {
				fsl,pins = <
					MX6UL_PAD_UART2_TX_DATA__UART2_DTE_RX	0x1b0b1 /* SODIMM 36 */
					MX6UL_PAD_UART2_RX_DATA__UART2_DTE_TX	0x1b0b1 /* SODIMM 38 */
				>;
			};
			
			pinctrl_ecspi1_cs: ecspi1-cs-grp {
				fsl,pins = <
					MX6UL_PAD_LCD_DATA21__ECSPI1_SS0	0x70a0 /* SODIMM 86 */
				>;
			};

			pinctrl_ecspi1: ecspi1-grp {
				fsl,pins = <
					MX6UL_PAD_LCD_DATA20__ECSPI1_SCLK	0x000a0 /* SODIMM 88 */
					MX6UL_PAD_LCD_DATA23__ECSPI1_MISO	0x000a0 /* SODIMM 90 */
					MX6UL_PAD_LCD_DATA22__ECSPI1_MOSI	0x100a0 /* SODIMM 92 */	
				>;
			};
			
			pinctrl_OEM719_gpio1: OEM719-gpio1-grp {   //OEM719
				fsl,pins = <
					MX6UL_PAD_BOOT_MODE0__GPIO5_IO10	0x10b0 /* SODIMM 105 */
					MX6UL_PAD_SNVS_TAMPER4__GPIO5_IO04	0x10b0 /* SODIMM 107 */
					MX6UL_PAD_UART3_CTS_B__GPIO1_IO26	0x10b0 /* SODIMM 100 */
					MX6UL_PAD_JTAG_TRST_B__GPIO1_IO15	0x10b0 /* SODIMM 102 */
					MX6UL_PAD_ENET1_RX_ER__GPIO2_IO07	0x10b0 /* SODIMM 104 */
					MX6UL_PAD_JTAG_MOD__GPIO1_IO10		0x10b0 /* SODIMM 106 */
					MX6UL_PAD_CSI_DATA00__GPIO4_IO21	0x10b0 /* SODIMM 101 */
					MX6UL_PAD_CSI_DATA01__GPIO4_IO22	0x10b0 /* SODIMM 103 */
				>;
			};
			
			pinctrl_usbh_reg: gpio-usbh-reg {
				fsl,pins = <
					MX6UL_PAD_GPIO1_IO02__GPIO1_IO02	0x0f058 /* SODIMM 129 */
					MX6UL_PAD_SNVS_TAMPER5__GPIO5_IO05	0x1b0b0 /* SODIMM 131 */
				>;
			};
		};
};